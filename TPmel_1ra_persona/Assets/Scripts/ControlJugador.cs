using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;
    public GameObject proyectil;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento; 
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento; 
        
        movimientoAdelanteAtras *= Time.deltaTime; 
        movimientoCostados *= Time.deltaTime;
        
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras); 
        
        if (Input.GetKeyDown("escape")) 
        { 
            Cursor.lockState = CursorLockMode.None; 
        }

        if (Input.GetMouseButtonDown(0)) 
        { 
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); 
            RaycastHit hit; 

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5) 
            { 
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);

                if (hit.collider.name.Substring(0, 3) == "Bot") 
                { 
                    GameObject objetoTocado = GameObject.Find(hit.transform.name); 
                    ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot)); 
                    
                    if (scriptObjetoTocado != null) 
                    { 
                        scriptObjetoTocado.recibirDa�o(); 
                    } 
                }
            } 
        }

        if (Input.GetMouseButtonDown(0)) 
        { 
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); 
            
            GameObject pro; 
            pro = Instantiate(proyectil, ray.origin, transform.rotation); 
            
            Rigidbody rb = pro.GetComponent<Rigidbody>(); 
            rb.AddForce(camaraPrimeraPersona.transform.forward * 35, ForceMode.Impulse);
            
            Destroy(pro, 5); 
        }

    }
}
